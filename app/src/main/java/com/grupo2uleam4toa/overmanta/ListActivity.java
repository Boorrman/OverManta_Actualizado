package com.grupo2uleam4toa.overmanta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {
    private List<lugar> items = new ArrayList<>();
    private List<tipoLugar> itemsT = new ArrayList<>();
    private RecyclerView recycler;
    private  RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        FillTlugar();


        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        adapter = new lugarAdapter(items);
        recycler.setAdapter(adapter);

        int opcion = getIntent().getExtras().getInt("opcion");

        switch (opcion){
            case 0:
                FilllugarT();
                break;
            case 1:
                FilllugarP();
                break;
            case 2:
                FilllugarR();
                break;
            case 3:
                FilllugarB();
                break;
            case 4:
                FilllugarH();
                break;
        }

    }

    private void FilllugarT(){
        items.add(new lugar(1, 1,R.drawable.lt01, "Un lugar turistico", "muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1muy buen lugar para turistear 1", -0.947417, -80.721746));
        items.add(new lugar(2, 1,R.drawable.lt02, "Dos lugares turisticos", "muy buen lugar para turistear 2", -0.941848, -80.730457));
    }

    private void FilllugarP(){
        items.add(new lugar(1, 1,R.drawable.p01, "Una playa", "una playa", 0.1230, 0.123));
        items.add(new lugar(2, 1,R.drawable.p02, "Dos playas", "dos playas", 0.1230, 0.123));
    }

    private void FilllugarR(){
        items.add(new lugar(1, 1,R.drawable.r01, "Un restaurante", "wuen restaurante", 0.1230, 0.123));
        items.add(new lugar(2, 1,R.drawable.r02, "Dos restaurantes", "otro wuen restaurante", 0.1230, 0.123));
    }
    private void FilllugarB(){
        items.add(new lugar(1, 2,R.drawable.b01, "Un bar", "muy buen lugar", 0.1230, 0.123));
        items.add(new lugar(2, 2,R.drawable.b02, "Dos bares", "muy buen lugar", 0.1230, 0.123));
        items.add(new lugar(3, 2,R.drawable.b03, "Tres bares", "muy buen lugar", 0.1230, 0.123));
        items.add(new lugar(4, 2,R.drawable.b04, "Cuatro bares", "muy buen lugar", 0.1230, 0.123));

    }
    private void FilllugarH(){
        items.add(new lugar(1, 1,R.drawable.h01, "Un hotel", "muy buen lugar numero 1", 0.1230, 0.123));
        items.add(new lugar(2, 1,R.drawable.h02, "Dos hoteles", "muy buen lugar numero 2", 0.1230, 0.123));
        items.add(new lugar(3, 1,R.drawable.h03, "Tres hoteles", "muy buen lugar numero 3", 0.1230, 0.123));
        items.add(new lugar(4, 1,R.drawable.h04, "Cuatro hoteles", "muy buen lugar numero 4", 0.1230, 0.123));

    }

    private void FillTlugar(){
        itemsT.add(new tipoLugar(0, "lugares turísticos"));
        itemsT.add(new tipoLugar(1, "playas"));
        itemsT.add(new tipoLugar(2, "restaurantes"));
        itemsT.add(new tipoLugar(3, "bares"));
        itemsT.add(new tipoLugar(4, "hoteles"));

    }
}
