package com.grupo2uleam4toa.overmanta;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class lugarAdapter extends RecyclerView.Adapter<lugarAdapter.lugarViewHolder> {
    private List<lugar> items;

    public static class lugarViewHolder extends  RecyclerView.ViewHolder{
        public CardView lugarCard;
        public ImageView imageViewLugar;
        public TextView textViewNombreLugar;


        public lugarViewHolder(View v){
            super(v);
            lugarCard = (CardView) v.findViewById(R.id.activity_list_row);
            imageViewLugar = (ImageView) v.findViewById(R.id.imageViewLugar);
            textViewNombreLugar = (TextView) v.findViewById(R.id.textViewNombreLugar);

        }
    }

    @NonNull
    @Override
    public lugarViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.activity_list_row, viewGroup, false);
        return new lugarViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final lugarViewHolder viewHolder, final int i) {
        viewHolder.imageViewLugar.setImageResource(items.get(i).getFoto());
        viewHolder.textViewNombreLugar.setText(String.valueOf(items.get(i).getNombre()));

        viewHolder.lugarCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("detImagen", items.get(i).getFoto());
                bundle.putString("detNombre", items.get(i).getNombre());
                bundle.putString("detDescripcion", items.get(i).getDescripcion());
                bundle.putDouble("detLat",items.get(i).getLat());
                bundle.putDouble("detLng",items.get(i).getLng());
                Intent intent = new Intent(v.getContext(), DetailActivity.class);
                intent.putExtras(bundle);
                v.getContext().startActivity(intent);


            }
        });

    }

    @Override
    public int getItemCount() {return items.size(); }

    public List<lugar> getItems() {
        return this.items;
    }

    public lugarAdapter(List<lugar> items) {this.items = items; }
}




